organization := "com.mercator"

name := "mercator-shopping-cart"

version := "1.0"

scalaVersion := "2.11.6"

resolvers ++= Seq(
  "Maven Repository" at "https://repo1.maven.org/maven2/",
  "Java.net Maven2 Repository" at "http://download.java.net/maven/2/"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.1" % "test"
)

fork in run := true
