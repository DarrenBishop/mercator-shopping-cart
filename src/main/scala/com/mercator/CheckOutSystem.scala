package com.mercator

/**
 * Created by Darren on 11/05/2015.
 */
object CheckOutSystem {

  case class Item(name: String)

  type ShoppingCart = List[Item]

  object ShoppingCart {

    def apply(): ShoppingCart = Nil

    def apply(shoppingList: String): ShoppingCart = {

      if (shoppingList.trim == "") ShoppingCart()
      else shoppingList.trim.split(",").foldRight(ShoppingCart()) { case (itemName, cart) => Item(itemName) :: cart }
    }
  }

  type PriceList = Map[Item, Double]

  object PriceList {

    val pencePriceRE = """(\d{2})p""".r
    val poundPriceRE = """\u00A3(\d+\.\d{2})""".r

    def apply(): PriceList = Map.empty

    def apply(prices: String): PriceList = {

      if (prices.trim == "") PriceList()
      else prices.trim.split(",").sliding(2, 2).foldLeft(PriceList()) { (pricesList, a) =>

        val price = a(1) match {
          case pencePriceRE(p) => p.toDouble / 100
          case poundPriceRE(p) => p.toDouble
        }
        pricesList + (Item(a(0)) -> price)
      }
    }
  }

  type Offers = Map[Item, Offer]

  object Offers {

    def apply(): Offers = Map.empty.withDefaultValue(NoOffer)

    def apply(offers: String): Offers = {

      if (offers.trim == "") Offers()
      else offers.trim.split(",").sliding(2, 2).foldLeft(Offers()) { (offers, a) =>

        val offer = a(1) match {
          case BOGOF() => BOGOF
          case ThreeForTwo() => ThreeForTwo
          case _ => NoOffer
        }
        offers + (Item(a(0)) -> offer)
      }
    }
  }
  
  sealed abstract class Offer(deal: (Int) => Int) {

    def apply(items: List[Item]): Int = deal(items.length)

    def unapply(name: String):Boolean = name.toLowerCase == toString.toLowerCase
  }

  case object NoOffer extends Offer(n => n)
  case object BOGOF extends Offer(n => n / 2 + n % 2)
  case object ThreeForTwo extends Offer(n => n / 3 * 2 + n % 3  / 2)

  object Till {

    def totalCost(priceList: PriceList, cart: ShoppingCart, offers: Offers = Offers()): String = {

      val groupedCart = cart.groupBy { case item => item }

      val itemTotals = groupedCart.map { case (item, items) => offers(item)(items) * priceList(item) }

      val total = itemTotals.foldLeft(0D) {case (total, price) => total + price }

      if (total < 1.0) f"${total*100}%.0fp"
      else f"\u00A3$total%.2f"
    }
  }

  def apply(args: Array[String]): String = {

    require(args.length == 2 || args.length == 3, "Please specify comma separated <shopping-list> <prices> [<offers>]")

    val shoppingCart = ShoppingCart(args(0))

    val priceList = PriceList(args(1))

    val offers = if (args.length == 3) Offers(args(2)) else Offers()

    val totalCost = Till.totalCost(priceList, shoppingCart, offers)

    totalCost
  }

  def main(args: Array[String]): Unit = {

    val totalCost = CheckOutSystem(args)

    println(totalCost)
  }
}
