package com.mercator

import org.scalatest._

/**
 * Created by Darren on 11/05/2015.
 */
class CheckOutSystemSuite extends FunSuite {

  import CheckOutSystem._

  val shoppingList = "Apple,Apple,Orange,Apple"

  val shoppingCart = ShoppingCart(shoppingList)

  val prices = "Apple,60p,Orange,25p"

  val priceList = PriceList(prices)

  val offers = "Apple,BOGOF"

  test("Shopping cart should be empty with an empty shopping list") {

    val shoppingList = ""

    val shoppingCart = ShoppingCart(shoppingList)

    assert(shoppingCart === List())
  }

  test("Shopping cart should contain an Apple, an Apple, an Orange and an Apple") {

    assert(shoppingCart === List(Item("Apple"), Item("Apple"), Item("Orange"), Item("Apple")))
  }

  test("Price list should contain Apple at 60p, an Orange at 25p and a Mango at \u00A31.30") {

    val prices = "Apple,60p,Orange,25p,Mango,\u00A31.30"

    val priceList = PriceList(prices)

    assert(priceList === Map(Item("Apple") -> 0.60, Item("Orange") -> 0.25, Item("Mango") -> 1.30))
  }

  test("An empty shopping cart should total 0p") {

    val shoppingCart = ShoppingCart()

    assert(Till.totalCost(priceList, shoppingCart) === "0p")
  }

  test("A shopping cart with 2 oranges should total 50p") {

    val shoppingCart = Item("Orange") :: Item("Orange") :: ShoppingCart()

    assert(Till.totalCost(priceList, shoppingCart) === "50p")
  }

  test("Shopping cart containing an Apple, an Apple, an Orange and an Apple should total \u00A32.05") {

    assert(Till.totalCost(priceList, shoppingCart) === "\u00A32.05")
  }

  test("Altogether now...") {

    val totalCost = CheckOutSystem(Array(shoppingList, prices))

    assert(totalCost === "\u00A32.05")
  }

  test("Introduce by-one-get-one-free offer on Apples") {

    val totalCost = CheckOutSystem(Array(shoppingList, prices, offers))

    assert(totalCost === "\u00A31.45")
  }

  test("Introduce by-one-get-one-free offer on Apples and 3 for the price of 2 on Oranges") {

    val shoppingList = "Apple,Apple,Orange,Apple,Orange,Orange"

    val offers = "Apple,BOGOF,Orange,ThreeForTwo"

    val totalCost = CheckOutSystem(Array(shoppingList, prices, offers))

    assert(totalCost === "\u00A31.70")
  }
}
